# wordpress-in-minikube

A repo that has all the details of running wordpress on minikube

The Dockerfile and entrypoint is copied from [here](https://github.com/docker-library/wordpress/tree/master/php7.2/apache)
